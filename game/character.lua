
character_type =
{
	none = 0,
	player = 1,
}

character_t = {}
character_t.__index = character_t

function character_t.new(type, x, y)
	ref = 
	{
		type = type,

		x = x,
		y = y,
		--vx = 0,
		vy = 0,
		--ax = 0,
		ay = 0,

		move = 0,
		move_from = 0,
		move_to = 0,
		move_speed = 7,

		can_move_left = 0,
		can_move_right = 0,
		stand = 0,

		direction = 1,

		squish = 1,
		died = false
	}

	return setmetatable(ref, character_t)
end

function math.sign(x)
   if x < 0 then
     return -1
   elseif x > 0 then
     return 1
   else
     return 0
   end
end

function character_t.update(self, world, dt)
	if self.stand == false then
		self.ay = 12
	else
		self.ay = 0
		self.vy = 0
		self.y = math.floor(self.y)
	end

	if self.move == 1 then
		self.move_from = math.floor(self.move_from)
		self.move_to = math.floor(self.move_to)
	
		old_x = self.move_from
		new_x = self.move_to

		if world:get_cube(new_x, self.y) == nil then
			self.x = self.x + math.sign(self.move_to - self.move_from) * self.move_speed * dt
			--self.move = 0
			--print(self.x .. " - " .. self.move_to .. " = " .. (self.x - self.move_to))
			if math.sign(self.move_to - self.move_from) * (self.x - self.move_to) >= 0 then
				self.move = 0
				self.x = math.floor(self.move_to)
			end
		else
			self.move = 0
			self.x = math.floor(old_x)
		end
	end

	--self.vx = self.vx + self.ax * dt
	self.vy = self.vy + self.ay * dt

	--self.x = self.x + self.vx * dt
	self.y = self.y + self.vy * dt
end