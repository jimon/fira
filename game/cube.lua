
cube_type =
{
	none		= 0,
	type_1		= 1,
	type_2		= 2,
	type_3		= 3,
	type_4		= 4,
	type_5		= 5,
}

--[[
cube_life =
{
	[cube_type.type_1] = 0.5,
	[cube_type.type_2] = 1.0,
	[cube_type.type_3] = 1.5,
	[cube_type.type_4] = 2.0,
	[cube_type.type_5] = 2.5
}
]]--
cube_life =
{
	[cube_type.type_1] = 0.4,
	[cube_type.type_2] = 0.4,
	[cube_type.type_3] = 0.4,
	[cube_type.type_4] = 0.4,
	[cube_type.type_5] = 0.4
}

cube_t = {}
cube_t.__index = cube_t

function cube_t.new(type, x, y, anchor)
	if anchor == nil then
		anchor = false
	end

	ref = 
	{
		type = type,
		x = x,
		y = y,
		vy = 0,
		ay = 0,
		falling = false,
		life = cube_life[type],
		cracks = 0,
		anchor = anchor
	}

	return setmetatable(ref, cube_t)
end

function cube_t.hit_cube(self, dt)
	self.life = self.life - 2.0 * dt
	--print(self.life .. " " .. dt)
	self.cracks = math.floor((1.0 - self.life / cube_life[self.type]) * 3) + 1
	if self.cracks > 4 then
		self.cracks = 4
	end
end