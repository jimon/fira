
require "cube"
require "character"
require "world"

function love.load()
	love.graphics.setDefaultFilter("nearest", "nearest", 1 )
	img_background = love.graphics.newImage("art/dd_underground_back.png")
	img_background:setWrap("repeat", "repeat")
	img_background_quad = love.graphics.newQuad(0, 0, 2048, 2048, img_background:getWidth(), img_background:getHeight())

	img_player = love.graphics.newImage("art/dd_player.png")
	img_player_quad = love.graphics.newQuad(0, 0, 64, 64, img_player:getWidth(), img_player:getHeight())

	img_player_dig = {}
	for i = 1, 3 do
		img_player_dig[i] = love.graphics.newQuad(64 * i, 0, 64, 64, img_player:getWidth(), img_player:getHeight())
	end
	img_player_dig_time = 0

	img_cubes_all = love.graphics.newImage("art/dd_blocks.png")

	img_cubes = {}
	img_cubes[cube_type.type_1] = love.graphics.newQuad(16 * 0, 16 * 0, 16, 16, img_cubes_all:getWidth(), img_cubes_all:getHeight())
	img_cubes[cube_type.type_2] = love.graphics.newQuad(16 * 1, 16 * 0, 16, 16, img_cubes_all:getWidth(), img_cubes_all:getHeight())
	img_cubes[cube_type.type_3] = love.graphics.newQuad(16 * 2, 16 * 0, 16, 16, img_cubes_all:getWidth(), img_cubes_all:getHeight())
	img_cubes[cube_type.type_4] = love.graphics.newQuad(16 * 3, 16 * 0, 16, 16, img_cubes_all:getWidth(), img_cubes_all:getHeight())
	img_cubes[cube_type.type_5] = love.graphics.newQuad(16 * 4, 16 * 0, 16, 16, img_cubes_all:getWidth(), img_cubes_all:getHeight())

	img_cracks_all = love.graphics.newImage("art/dd_cracks.png")
	img_cracks = {}
	img_cracks[1] = love.graphics.newQuad(16 * 0, 16 * 1, 16, 16, img_cracks_all:getWidth(), img_cracks_all:getHeight())
	img_cracks[2] = love.graphics.newQuad(16 * 0, 16 * 2, 16, 16, img_cracks_all:getWidth(), img_cracks_all:getHeight())
	img_cracks[3] = love.graphics.newQuad(16 * 0, 16 * 3, 16, 16, img_cracks_all:getWidth(), img_cracks_all:getHeight())
	img_cracks[4] = love.graphics.newQuad(16 * 0, 16 * 4, 16, 16, img_cracks_all:getWidth(), img_cracks_all:getHeight())

	--next_block = 10
	block_height = 16
	current_block = 1

	world = world_t.new()
	world:spawn_static_cubes(current_block, current_block + block_height)
	--world:spawn_falling_cubes_test()
	player = world:spawn_character(character_type.player, 1, -1)

	test_param = 0
end

function love.update(dt)
	world:update(dt)

	if player.y > current_block + block_height / 2 then
		world:remove_static_cubes(current_block - block_height - 1, current_block - 1)

		current_block = current_block + block_height

		world:spawn_static_cubes(current_block, current_block + block_height)


	end

	if player.died then
	elseif love.keyboard.isDown("left") then
		if player.move == 0 then
			player.direction = -1;
			if player.can_move_left then
				player.move = 1
				player.move_from = player.x
				player.move_to = player.x - 1
			else
				world:hit_cube(math.floor(player.x) - 1, math.floor(player.y), dt)
			end
		end
	elseif love.keyboard.isDown("right") then
		if player.move == 0 then
			player.direction = 1;
			if player.can_move_right then
				player.move = 1
				player.move_from = player.x
				player.move_to = player.x + 1
				--print("right " .. tostring(player.move_from) .. " -> " .. tostring(player.move_to))
			else
				world:hit_cube(math.floor(player.x) + 1, math.floor(player.y), dt)
			end
		end
	elseif love.keyboard.isDown("down") then
		if player.stand then
			world:hit_cube(math.floor(player.x), math.floor(player.y) + 1, dt)
		end
	elseif love.keyboard.isDown("up") then
		if player.stand then
			world:hit_cube(math.floor(player.x), math.floor(player.y) - 1, dt)
		end
	end
end

function img_draw_cntr(img, x, y, r, sx, sy)
	if sx == nil then sx = 1 end
	if sy == nil then sy = sx end
	love.graphics.draw(img, x, y, r, sx, sy, img:getWidth() / 2, img:getHeight() / 2)
end

function love.draw()
	--wr, hr = love.graphics.getDimensions()
	--love.graphics.scale(wr / w, hr / h)
	love.window.setTitle("Fira")

	love.graphics.setColor(255, 255, 255, 255)

	s = 4
	cam_pos_x = 64
	cam_pos_y = 200 - player.y * 16 * s

	love.graphics.setBackgroundColor(64, 64, 64)
	love.graphics.clear()

	local bg_s = 3
	love.graphics.draw(img_background, img_background_quad, 0, ((cam_pos_y / 2) % (32 * bg_s)) - 32 * bg_s, 0, bg_s, bg_s)

	for x, tv in pairs(world.static_cubes) do
		for y, cube in pairs(tv) do
			love.graphics.draw(img_cubes_all, img_cubes[cube.type], (x) * 16 * s + cam_pos_x, y * 16 * s + cam_pos_y, 0, 4, 4, 8, 8)
		end
	end
	for x, tv in pairs(world.static_cubes) do
		for y, cube in pairs(tv) do
			if cube.cracks > 0 then
				love.graphics.draw(img_cracks_all, img_cracks[cube.cracks], (x) * 16 * s + cam_pos_x, y * 16 * s + cam_pos_y, 0, 4, 4, 8, 8)
			end
			if cube.anchor then
				love.graphics.print("anchor " .. tostring(cube.anchor_id), (x) * 16 * s + cam_pos_x - 16, y * 16 * s + cam_pos_y)
			else
				love.graphics.print(tostring(cube.anchor_distance) .. " " .. tostring(cube.anchor_weight), (x) * 16 * s + cam_pos_x, y * 16 * s + cam_pos_y)
			end
		end
	end
	for index, cube in pairs(world.falling_cubes) do
		love.graphics.draw(img_cubes_all, img_cubes[cube.type], (cube.x) * 16 * s + cam_pos_x, cube.y * 16 * s + cam_pos_y, 0, 4, 4, 8, 8)
	end
	for index, cube in pairs(world.falling_cubes) do
		if cube.cracks > 0 then
			love.graphics.draw(img_cracks_all, img_cracks[cube.cracks], (cube.x) * 16 * s + cam_pos_x, cube.y * 16 * s + cam_pos_y, 0, 4, 4, 8, 8)
		end
	end

	if player_moving == 1 then
		love.graphics.draw(img_player, img_player_dig[math.floor(img_player_dig_time) + 1], (player.x) * 16 * s + cam_pos_x, player.y * 16 * s + cam_pos_y, 0, 4 * player.direction, 4, 32, 32)
	else
		love.graphics.draw(img_player, img_player_quad,
			(player.x) * 16 * s + cam_pos_x,
			player.y * 16 * s + cam_pos_y + 32 * (1 - player.squish),
			0,
			4 * player.direction,
			4 * player.squish,
			32,
			32)
	end

	love.graphics.print("Current FPS: " .. tostring(love.timer.getFPS()), 10, 10)
	love.graphics.print("Deep: " .. tostring(math.floor(player.y * 10) / 10)  .. " meters", 10, 20)

	if player.died then
		love.graphics.print("DIED", 100, 100)
	end
end

function love.keypressed(key)
	if key == "escape" then
		love.event.quit()
	end

	if key == "r" then test_param = test_param + 1 end
	if key == "f" then test_param = test_param - 1 end
end
