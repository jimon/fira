
require "cube"
require "character"

world_t = {}
world_t.__index = world_t

function world_t.new()
	ref = 
	{
		static_cubes = {},
		falling_cubes = {},
		characters = {},
		right = 10
	}

	for x = 1, ref.right do
		ref.static_cubes[x] = {}
	end

	return setmetatable(ref, world_t)
end

function world_t.spawn_static_cubes_circle(self, ox, oy, ty, by, r, type)
	for x = ox - r, ox + r do
		for y = oy - r, oy + r do
			if  x >=1
				and x <= self.right
				and y >= ty
				and y <= by
				and math.sqrt((x - ox) * (x - ox) + (y - oy) * (y - oy)) <= r
				then
				self.static_cubes[x][y] = cube_t.new(type, x, y)

				if math.random(1, 80) == 1 then
					self.static_cubes[x][y].anchor = true
				end
			end
		end
	end
end

function world_t.spawn_static_cubes(self, top_y, bottom_y)
	for i = 1, (bottom_y - top_y) * 2 do
		type = math.random(cube_type.type_1, cube_type.type_5)

		self:spawn_static_cubes_circle(
			math.random(1, self.right),
			math.random(top_y, bottom_y),
			top_y,
			bottom_y,
			math.random(1, (7 - type) / 2),
			type)
	end

	--[[
	for x = 1, 10 do
		self.static_cubes[x] = {}
		for y = 1, 4 do
			self.static_cubes[x][y] = cube_t.new(math.random(cube_type.type_1, cube_type.type_5), x, y)
			if self.static_cubes[x][y].type == cube_type.none then
				self.static_cubes[x][y] = nil
			end
		end
		for y = 10, 15 do
			self.static_cubes[x][y] = cube_t.new(math.random(cube_type.type_1, cube_type.type_5), x, y)
			if self.static_cubes[x][y].type == cube_type.none then
				self.static_cubes[x][y] = nil
			end
		end
	end

	self.static_cubes[4][1] = nil
	self.static_cubes[4][2] = nil
	self.static_cubes[4][3] = nil
	self.static_cubes[4][4] = nil
	self.static_cubes[1][1] = nil
	--self.static_cubes[1][2] = nil
	self.static_cubes[1][3] = nil
	self.static_cubes[1][4] = nil
	]]--
end

function world_t.remove_static_cubes(self, top_y, bottom_y)
	for x = 1, self.right do
		for y = top_y, bottom_y do
			if self:get_cube(x, y) then
				self.static_cubes[x][y] = nil
			end
		end
	end
end

function world_t.spawn_falling_cubes_test(self)
	for x = 2, 2 do
		for y = -1, -1 do
			self.falling_cubes[#self.falling_cubes + 1] = cube_t.new(math.random(cube_type.type_2, cube_type.type_2), x, y)
		end
		for y = -3, -3 do
			self.falling_cubes[#self.falling_cubes + 1] = cube_t.new(math.random(cube_type.type_2, cube_type.type_2), x, y)
		end
		for y = -5, -5 do
			self.falling_cubes[#self.falling_cubes + 1] = cube_t.new(math.random(cube_type.type_2, cube_type.type_2), x, y)
		end
	end
end

function world_t.spawn_character(self, type, x, y)
	character = character_t.new(type, x, y)
	self.characters[#self.characters + 1] = character
	return character
end

function world_t.hit_cube(self, x, y, dt)
	cube = self:get_cube(x, y)
	if cube then
		if cube.life > 0 then
			cube:hit_cube(dt)
		else
			world.static_cubes[x][y] = nil
		end
	end
end

function world_t.get_cube(self, x, y)
	if x < 1 or x > self.right or y < 1 then
		return nil
	elseif self.static_cubes[x] ~= nil then
		return self.static_cubes[x][y]
	else
		return nil
	end
end

function world_t.update_falling_cubes(self, dt)
	sorted = {}

	for index, cube in pairs(self.falling_cubes) do
		sorted[#sorted + 1] = cube
		cube.falling_index = index
	end

	table.sort(sorted, function(cube_1, cube_2) return cube_1.y < cube_2.y end)

	for i, cube in ipairs(sorted) do
		cube.ay = 12
		cube.vy = cube.vy + cube.ay * dt
		cube.y = cube.y + cube.vy * dt

		x = math.floor(cube.x)
		y = math.floor(cube.y)
		if self:get_cube(x, y + 1) ~= nil then
			cube.ay = 0
			cube.vy = 0
			cube.y = y
			self.static_cubes[x][y] = cube
			table.remove(self.falling_cubes, cube.falling_index)
		end
	end

	for key, character in pairs(self.characters) do
		if not character.died then
			x = math.floor(character.x)
			y = math.floor(character.y)
			character.squish = 1

			for index, cube in pairs(self.falling_cubes) do
				x2 = math.floor(cube.x)
				y2 = math.floor(cube.y)

				if (x == x2) and (y == y2 + 1) then
					local s = character.y - cube.y
					if s < character.squish then
						character.squish = s
						--print("killed ? " .. s)
					end
				end
			end
		end
	end
end

function world_t.figure_out_falling_cubes(self, dt)
	local anchored = {}

	for x, tv in pairs(self.static_cubes) do
		for y, cube in pairs(tv) do
			if cube.anchor then
				cube.anchored = true
				cube.anchor_distance = 0
				cube.anchor_id = #anchored + 1
				cube.anchor_attached = -1
				cube.anchor_parent = -1
				cube.anchor_weight = 0
				anchored[#anchored + 1] = cube
			else
				cube.anchored = false
				cube.anchor_distance = -1
				cube.anchor_parent = -1
				cube.anchor_id = -1
				cube.anchor_attached = -1
				cube.anchor_weight = 0
			end
		end
	end

	local run = true
	while run do
		run = false
		for index, cube in pairs(anchored) do
			local f = function(world, cube, x, y)
				test = world:get_cube(x, y)
				if test then
					exist = false
					for i, c in pairs(anchored) do
						if c == test then
							exist = true
							break
						end
					end
					if not exist then
						run = true
						test.anchored = true
						test.anchor_id = #anchored + 1
						test.anchor_distance = cube.anchor_distance + 1

						p = cube.anchor_id
						while p ~= -1 do
							t = anchored[p]
							t.anchor_weight = t.anchor_weight + 1
							p = t.anchor_parent
						end

						test.anchor_parent = cube.anchor_id
						if cube.anchor_attached == -1 then
							test.anchor_attached = cube.anchor_id
						else
							test.anchor_attached = cube.anchor_attached
						end
						anchored[#anchored + 1] = test
					end
				end
			end
			x = math.floor(cube.x)
			y = math.floor(cube.y)
			f(self, cube, x + 1, y)
			f(self, cube, x - 1, y)
			f(self, cube, x , y + 1)
			f(self, cube, x , y - 1)
		end
	end

	--print("loop")
	for x, tv in pairs(self.static_cubes) do
		for y, cube in pairs(tv) do
			if cube.anchored ~= true then
				cube.ay = 0
				cube.vy = 0
				cube.y = math.floor(y)				
				self.falling_cubes[#self.falling_cubes + 1] = cube
				self.static_cubes[x][y] = nil
				--print("falling ! " .. cube.x .. " " .. cube.y)
			--elseif cube.anchor_weight > 0 and not cube.anchor then
			--	self:hit_cube(cube.x, cube.y, dt / 10)
			end
		end
	end

	lowest = {}


	for index, cube in pairs(anchored) do
		local k = cube.anchor_attached
		if lowest[k] == nil then
			local t = {}
			for x = 1, self.right do
				t[x] = {y = cube.y, index = index}
			end
			lowest[k] = t
		end


	end
end

function world_t.update_characters(self, dt)
	for key, character in pairs(self.characters) do
		character:update(self, dt)

		x = math.floor(character.x)
		y = math.floor(character.y)

		y2 = character.y - math.floor(character.y)

		character.can_move_left = self:get_cube(x - 1, y) == nil
		character.can_move_right = self:get_cube(x + 1, y) == nil

		if math.abs(y2) > 0.01 then
			character.can_move_left = character.can_move_left and (self:get_cube(x - 1, y + 1) == nil)
			character.can_move_right = character.can_move_right and (self:get_cube(x + 1, y + 1) == nil)
		end

		if x == 1 then character.can_move_left = false end
		if x == self.right then character.can_move_right = false end

		character.stand = self:get_cube(x, y + 1) ~= nil

		if character.squish < 0.2 then
			character.died = true
		end
	end
end

function world_t.update(self, dt)
	--dt = dt / 5
	self:update_characters(dt)
	self:update_falling_cubes(dt)
	self:figure_out_falling_cubes(dt)
end